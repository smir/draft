#!/home/samir/j901/bin/jconsole

require 'jzplot tables/csv viewmat stats web/gethttp plot'
NB. load '~Demos/wdplot/plotdemos.ijs'
NB. plotdemos n where n 1:54

NB.------------------
NB. Intra-day
NB.------------------
intra =: 3 : 0
  stock =: fixcsv gethttp dquote 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=',y,'&interval=5min&apikey=MAVGPJN3IXFBAUL8&datatype=csv'

  stock_data =: }. stock
  stock_head =: {. stock 
  'stock_date stock_close stock_high' =: <"1 |: stock_data {"1 ~ stock_head i. 'timestamp';'close'; 'high'

  stock_close =: makenum stock_close
  stock_high =: makenum stock_high

  'xtic 10; pensize 2' plot (|. stock_close) NB. ,: (|. stock_high) 
)

NB.--------------------
NB. Daily
NB.--------------------
day =: 3 : 0
  'stk'=. y
  stock =: fixcsv gethttp dquote 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=',stk,'&apikey=MAVGPJN3IXFBAUL8&datatype=csv'

  stock_data =: }. stock 
  stock_head =: {. stock 

  'stock_date stock_close stock_high' =: <"1 |: stock_data {"1 ~ stock_head i. 'timestamp';'close'; 'high'

  stock_close =: |: (makenum stock_close)
  stock_high =: |: (makenum stock_high)

  ma =. (+/%#)\
  s =. 20 ma stock_close

  'xtic 10; pensize 2; type dot' plot stock_close ,: ((0 ~: s)#s) 
)



